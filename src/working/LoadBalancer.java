package working;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by jiaweizhang on 2/8/2016.
 *
 * http://codeforces.com/problemset/problem/609/C
 */
public class LoadBalancer {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Integer> tasks = new ArrayList<Integer>();
        for (int i=0; i<n; i++) {
            tasks.add(sc.nextInt());
        }

        long seconds = 0;
        Collections.sort(tasks);
        double average = calculateAverage(tasks);

        int sum = tasks.stream().mapToInt(Integer::intValue).sum();

        long roundedAvg = Math.round(average);

        if (Math.ceil(average)==Math.round(average)) {
            // subtract from biggest n
            long num = Math.abs(sum - tasks.size() * roundedAvg);
            for (int i=0; i< num; i++) {
                tasks.set(i, tasks.get(i)+1);
            }

        } else if (Math.floor(average)==Math.round(average)) {
            // add to smallest n
            long num = Math.abs(sum - tasks.size() * roundedAvg);
            for (int i=tasks.size()-1; i>= tasks.size()-num; i--) {
                //System.out.printf("replacing %d at %d\n", tasks.get(i), i);
                tasks.set(i, tasks.get(i)-1);
            }
        }

        //System.out.println(tasks.toString());
        for (int i=0; i<tasks.size(); i++) {
            seconds = seconds + Math.abs(roundedAvg - tasks.get(i));
        }

        System.out.println(seconds/2);
    }

    private static double calculateAverage(List <Integer> marks) {
        Integer sum = 0;
        if(!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }
}

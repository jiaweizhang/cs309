package working;

import java.util.*;

/**
 * Created by jiaweizhang on 2/8/2016.
 */

public class USB {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        List<Integer> usbSizes = new ArrayList<Integer>();

        for (int i=0; i < n; i++) {
            usbSizes.add(sc.nextInt());
        }

        Collections.sort(usbSizes);

        int count = 0;
        for (int i=usbSizes.size() - 1; i>=0; i--) {
            m = m-usbSizes.get(i);
            count++;
            if (m<=0) {
                System.out.println(count);
                break;
            }
        }
    }
}
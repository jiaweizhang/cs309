import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by jiaweizhang on 2/22/16.
 *
 * http://cs.baylor.edu/~hamerly/icpc/qualifier_2013/final_problem_statements.pdf
 */
public class ErraticAnts {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int numTrails = sc.nextInt();
        for (int i=0; i<numTrails; i++) {
            int pathLength = sc.nextInt();
            List<String> directions = new ArrayList<String>();
            List<Point> points = new ArrayList<Point>();
            Map<Point, Set<String>> possibilities = new HashMap<Point, Set<String>>();
            for (int j=0; j<pathLength; j++) {
                directions.add(sc.next());
            }
            int x = 0;
            int y = 0;
            for (int j=0; j<directions.size(); j++) {
                if (directions.get(j).equals("N")) {
                    y++;
                } else if (directions.get(j).equals("W")) {
                    x--;
                } else if (directions.get(j).equals("E")) {
                    x++;
                } else if (directions.get(j).equals("S")) {
                    y--;
                }
                points.add(new Point(x,y));
                if (directions.get(j).equals("N")) {

                } else if (directions.get(j).equals("W")) {

                } else if (directions.get(j).equals("E")) {

                } else if (directions.get(j).equals("S")) {

                }
            }
        }
    }

}

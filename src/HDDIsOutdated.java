import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by jiaweizhang on 2/29/16.
 */
public class HDDIsOutdated {
    public static void main(String args[]) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        for (int i=1; i<=n; i++) {
            map.put(sc.nextInt(), i);
        }

        int current = 1;
        long distance = 0;

        for (int i=2; i<=n ;i++) {
            distance += (Math.abs(map.get(i)-map.get(current)));
            current = i;
        }
        System.out.println(distance);

    }
}
